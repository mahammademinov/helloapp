package com.company.service.functional;

import com.company.constant.Message;
import com.company.entity.Student;
import com.company.exception.ResourceNotFoundException;
import com.company.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public void insertOrUpdate(Student student) {
        studentRepository.save(student);
    }

    public void insertOrUpdateList(Iterable<Student> students) {
        studentRepository.saveAll(students);
    }

    public Student findById(Long id) {
        return studentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(Message.STUDENT_NOT_FOUND.name()));
    }
}
