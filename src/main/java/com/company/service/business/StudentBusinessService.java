package com.company.service.business;

import com.company.dto.payload.StudentPayload;
import com.company.entity.Student;
import com.company.service.functional.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentBusinessService {

    private final StudentService studentService;

    public String sayHello(String name) {
        return "Hello " + name;
    }

    public Student findById(Long id) {
        return studentService.findById(id);
    }

    public void create(StudentPayload studentPayload) {
        Student student = studentPayload.toEntity();
        studentService.insertOrUpdate(student);
    }
}
