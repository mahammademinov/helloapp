package com.company.exception;

import lombok.*;
import org.springframework.http.HttpStatus;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ControllerException extends RuntimeException {

    private HttpStatus status;
    private String message;
}
