package com.company.dto.payload;

import com.company.entity.Student;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
public class StudentPayload {

    private String fullName;
    private String email;
    private Integer age;
    private BigDecimal scholarship;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthdate;

    public Student toEntity() {
        return Student.builder()
                .fullName(this.fullName)
                .email(this.email)
                .age(this.age)
                .scholarship(this.scholarship)
                .birthdate(this.birthdate)
                .build();
    }
}
