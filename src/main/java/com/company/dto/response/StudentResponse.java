package com.company.dto.response;

import com.company.entity.Student;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
public class StudentResponse {

    private Long id;
    private String fullName;
    private String email;
    private Integer age;
    private BigDecimal scholarship;
    private LocalDate birthdate;


    public static StudentResponse fromEntity(Student student) {
        return StudentResponse.builder()
                .id(student.getId())
                .fullName(student.getFullName())
                .age(student.getAge())
                .email(student.getEmail())
                .birthdate(student.getBirthdate())
                .build();
    }
}
