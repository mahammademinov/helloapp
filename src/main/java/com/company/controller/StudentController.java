package com.company.controller;

import com.company.dto.payload.StudentPayload;
import com.company.service.business.StudentBusinessService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentBusinessService studentBusinessService;

    @GetMapping("/{name}")
    public ResponseEntity<?> sayHello(@PathVariable(value = "name") String name) {
        return ResponseEntity.ok(studentBusinessService.sayHello(name));
    }

    @GetMapping
    public ResponseEntity<?> findById(@RequestParam(value = "id") Long id) {
        return ResponseEntity.ok(studentBusinessService.findById(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody StudentPayload studentPayload) {
        studentBusinessService.create(studentPayload);
    }
}
