FROM alpine:3.11.2
MAINTAINER Mahammad Eminov
RUN apk add --no-cache openjdk11
COPY build/libs/helloApp-0.0.1-SNAPSHOT.jar /app/app.jar
WORKDIR /app
ENTRYPOINT ["java"]
CMD ["-jar", "app.jar"]
